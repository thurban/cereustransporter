<?php
    /*******************************************************************************
     *
     * File:         $Id: CereusReporting_Reports.php,v ea43511c66ce 2018/11/11 17:22:55 thurban $
     * Modified_On:  $Date: 2018/11/11 17:22:55 $
     * Modified_By:  $Author: thurban $
     * Language:     Perl
     * Encoding:     UTF-8
     * Status:       -
     * License:      Commercial
     * Copyright:    Copyright 2009/2010 by Urban-Software.de / Thomas Urban
     *******************************************************************************/

    include_once( 'functions.php' );

    $dir = __DIR__;
    $mainDir = preg_replace( "@plugins.CereusTransporter@", "", $dir );
    chdir( $mainDir );
    include( "./include/auth.php" );
    $_SESSION[ 'custom' ] = FALSE;

    /* set default action */
    if ( !isset( $_REQUEST[ "drp_action" ] ) ) {
        $_REQUEST[ "drp_action" ] = "";
    }
    if ( !isset( $_REQUEST[ "sort_column" ] ) ) {
        $_REQUEST[ "sort_column" ] = "";
    }
    if ( !isset( $_REQUEST[ "sort_direction" ] ) ) {
        $_REQUEST[ "sort_direction" ] = "";
    }
    if ( !isset( $_REQUEST[ "Duplicate" ] ) ) {
        $_REQUEST[ "Duplicate" ] = "";
    }
    else {
        $_REQUEST[ "Duplicate" ] = "Duplicate";
    }
    if ( !isset( $_REQUEST[ "reportName" ] ) ) {
        $_REQUEST[ "reportName" ] = "";
    }
    if ( !isset( $_REQUEST[ "reportId" ] ) ) {
        $_REQUEST[ "reportId" ] = "";
    }

    if ( CEREUSTRANSPORTER_EDITION == "FREE" ){
        top_header();
        print "<b>This feature is only available on the professional version. Thank you.</b>";
        bottom_footer();
        return;
    }

    // CRC-7 - No Copy function at report page nor report schedule
    if ( $_REQUEST[ "Duplicate" ] == "Duplicate" ) {
        form_duplicate_execute();
    }

    switch ( $_REQUEST[ "drp_action" ] ) {
        case '3':
            top_header();
            form_duplicate();
            bottom_footer();
            break;
        case '2':
            form_delete();
            break;
        default:
            top_header();
            form_display();
            bottom_footer();
            break;
    }

    // CRC-7 - No Copy function at report page nor report schedule
    function form_duplicate_execute()
    {
        global $config, $colors;

        // Get DB Instance
        $db = DBCxn::get();
        $filterId = $_REQUEST[ "filterId" ];

        $sql = "Insert Into
		plugin_CereusTransporter_filter (
			filter_name,
			filter_description,
			filter_type_id,
			filter_active
		)
	Select
		:filterName,
        filter_description,
        filter_type_id,
        filter_active
	From
		plugin_CereusTransporter_filter
	Where
		filterId= :filterId;
	";
        $stmt = $db->prepare( $sql );
        $stmt->bindValue( ':filterName', $_REQUEST[ "filterName" ] );
        $stmt->bindValue( ':filterId', $filterId );
        $stmt->execute();
        $stmt->closeCursor();


        header( "Location: CereusTransporter_Filter.php" );
    }

    function form_duplicate()
    {
        global $config, $colors;

        $reportId = '';
        /* loop through each of the selected tasks and delete them*/
        foreach ( $_POST as $var => $val) {
            if ( preg_match( "/^chk_([0-9]+)$/", $var, $matches ) ) {
                /* ================= input validation ================= */
                input_validate_input_number( $matches[ 1 ] );
                /* ==================================================== */
                $reportId = $matches[ 1 ];
            }
        }

        $old_filter_name = db_fetch_cell( "SELECT filter_name FROM plugin_CereusTransporter_filter WHERE filterId=filterId" );

        $fields_cereusreporting_report_duplicate = array(
            "filterName" => array(
                "method"        => "textbox",
                "friendly_name" => "New Filter Name",
                "description"   => "A useful name for this Filter.",
                "value"         => $old_filter_name . '_new',
                "max_length"    => "255",
                "size"          => "60"
            ),
            "mode"       => array(
                "method" => "hidden",
                "value"  => "duplicate"
            ),
            "Duplicate"       => array(
                "method" => "hidden",
                "value"  => "Duplicate"
            ),
            "filterId"   => array(
                "method" => "hidden",
                "value"  => $reportId
            )
        );

        $type = "Duplicate";

        print "<table align='center' width='80%'><tr><td>\n";
        html_start_box( "<strong>CereusTransporter - " . htmlspecialchars( $type ) . " Filters</strong>", "100%", htmlspecialchars( $colors[ "header" ] ), "3", "center", "" );
        print "<tr><td bgcolor='#FFFFFF'>\n";

        print "<p>When you click 'Continue', the following Report will be duplicated. You can optionally change the title format for the new Report.</p>
		   <p>Press <b>'Duplicate'</b> to proceed with the duplication, or <b>'Cancel'</b> to return to the Reports menu.</p>
			</td></tr>";

        html_end_box();

        //print "<form action='CereusTransporter_Reports.php' method='post'>\n";
        html_start_box( "<strong>Report " . htmlspecialchars( $type ) . " Settings</strong>", "100%", htmlspecialchars( $colors[ "header" ] ), "3", "center", "" );
        draw_edit_form( array(
                "config" => array(),
                "fields" => inject_form_variables( $fields_cereusreporting_report_duplicate, array() ) )
        );
        html_end_box();
        cereusReporting_confirm_button( "Duplicate", "CereusTransporter_Reports.php" );
        print "</td></tr></table>\n";
    }

    function cereusReporting_confirm_button( $action, $cancel_url )
    {
        ?>
        <table align='center' width='100%' style='background-color: #ffffff; border: 1px solid #bbbbbb;'>
            <tr>
                <td bgcolor="#f5f5f5" align="right">
                    <input name='<?php print 'return' ?>' type='submit' value='Cancel'>
                    <input name='Duplicate' type='submit' value='Duplicate'>
                </td>
            </tr>
        </table>
        </form>
        <?php
    }

    function form_delete()
    {
        global $colors, $hash_type_names;

        /* loop through each of the selected tasks and delete them*/
        foreach ( $_POST as $var => $val) {
            if ( preg_match( "/^chk_([0-9]+)$/", $var, $matches ) ) {
                /* ================= input validation ================= */
                input_validate_input_number( $matches[ 1 ] );
                /* ==================================================== */
                // CRC-6 Deleting a report does not delete associated schedule
                db_execute( "DELETE FROM `plugin_CereusTransporter_filter` where `filterId`='" . $matches[ 1 ] . "'" );
            }
        }
        header( "Location: CereusTransporter_Filters.php" );
    }

    function form_edit()
    {

    }

    function form_display() {
        global $colors, $config;
        print "<font size=+1>CereusTransporter - Filters</font><br>\n";
        print "<hr>\n";

        $username = db_fetch_cell( "select username from user_auth where id=" . $_SESSION[ "sess_user_id" ] );

        $where_clause = '';
        if ( isset( $_REQUEST[ "sort_column" ] ) ) {
            if (
                ( $_REQUEST[ "sort_column" ] == 'ReportID' )
                || ( $_REQUEST[ "sort_column" ] == 'Name' )
                || ( $_REQUEST[ "sort_column" ] == 'Description' )
            ) {
                if (
                    ( $_REQUEST[ "sort_direction" ] == 'ASC' )
                    || ( $_REQUEST[ "sort_direction" ] == 'DESC' )
                ) {
                    $where_clause .= ' ORDER BY ' .
                        $_REQUEST[ "sort_column" ] .
                        ' ' . $_REQUEST[ "sort_direction" ];
                }
            }
        }
        $a_filters = db_fetch_assoc( "
            SELECT
              `plugin_CereusTransporter_filter`.`filterId`,
              `plugin_CereusTransporter_filter`.`filter_name`,
              `plugin_CereusTransporter_filter`.`filter_description`,
              `plugin_CereusTransporter_filter`.`filter_active`,
              `plugin_CereusTransporter_filter_type`.`filter_type_name` 
            FROM
              `plugin_CereusTransporter_filter` INNER JOIN
              `plugin_CereusTransporter_filter_type` ON `plugin_CereusTransporter_filter_type`.`filter_type_id`
                = `plugin_CereusTransporter_filter_type`.`filterTypeId`;
        " );

        print "<form name=chk method=POST action=CereusTransporter_Filter.php>\n";

        html_start_box( "<strong>CereusTransporter  - Filters</strong>", "100%", htmlspecialchars( $colors[ "header" ] ), "3", "center", "CereusTransporter_addFilter.php?action=add" );

        form_hidden_box( "save_component_import", "1", "" );

        if (sizeof( $a_filters ) > 0) {

            $menu_text = array(
                //"ID" => array("filterId", "ASC"),
                "filter_name"        => array( "Name", "ASC" ),
                "filter_description" => array( "Description", "ASC" ),
                "filter_type_name"  => array( "Type", "ASC" ),
                "filter_active"  => array( "is Active", "ASC" )
            );

            html_header_sort_checkbox( $menu_text, $_REQUEST[ "sort_column" ], $_REQUEST[ "sort_direction" ] );
            $limit = 100;
            foreach ($a_filters as $a_filter)
            {
                $description = $a_filter[ 'Description' ];
                $description = preg_replace( "/<br>/", "", $description );
                if ( strlen( $description ) > $limit ) {
                    $description = substr( $description, 0, strrpos( substr( $description, 0, $limit ), ' ' ) ) . '...';
                }
                $description = htmlspecialchars( $description );

                form_alternate_row('line' . $a_filter[ 'filterId' ], true);
                form_selectable_cell( "<a href='" . $config[ 'url_path' ] . "plugins/CereusTransporter/CereusTransporter_addFilter.php?action=update&filterId=" . $a_filter[ "filterId" ] . "'><img style='border:0px' src='" . $config[ 'url_path' ] . "plugins/CereusTransporter/images/Report.png'/><b>" . htmlspecialchars( $a_filter[ 'Name' ] ) . "</b></a>", $a_filter[ 'ReportId' ], 250 );
                form_selectable_cell( $description, $a_filter[ "filterId" ] );
                form_selectable_cell( $a_filter[ 'filter_type_name' ], $a_filter[ "filterId" ] );
                form_selectable_cell( $a_filter[ 'filter_active' ], $a_filter[ "filterId" ] );
                form_checkbox_cell( 'selected_items', $a_filter[ "filterId" ] );
                form_end_row();
            }
            html_end_box( FALSE );

            $task_actions = array(
                1 => "Please select an action",
                2 => "Delete",
                3 => "Duplicate"
            );
            draw_actions_dropdown( $task_actions );
        } else {
            print "<tr>
                <td><em>No Filter exist</em></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>";
            html_end_box( FALSE );
        }
        print "</form>";
    }
