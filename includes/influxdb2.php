<?php
    /*******************************************************************************
     *
     * File:         $Id$
     * Modified_On:  $Date$
     * Modified_By:  $Author$
     * License:      Commercial
     * Copyright:    Copyright 2009-2020 by Urban-Software.de / Thomas Urban
     *******************************************************************************/

    function CereusTransporter_influxdb2_send_data( $data_array )
    {
        $db_url    = read_config_option( 'cereus_transporter_db_fullurl' );
        $db_server = read_config_option( 'cereus_transporter_db_server' );
        $db_token  = read_config_option( 'cereus_transporter_db_token' );
        $db_org    = read_config_option( 'cereus_transporter_db_org' );
        $db_bucket = read_config_option( 'cereus_transporter_db_bucket' );
        $db_type   = read_config_option( 'cereus_transporter_dbtype' );

        // curl for bosun and opentsdb
        $client = NULL;

        // Disable error reporting for the DB Connection Creation
        $original_error_reporting = error_reporting();
        error_reporting( 0 );

        // autoload dependencies ( namely InfluxDB client )
        require_once __DIR__ . '/../vendor/autoload.php';
        // Connect to the remote InfluxDB host and fetch the database

        try {
            $client = new InfluxDB2\Client( [
                "url"       => $db_url,
                "token"     => $db_token,
                "bucket"    => $db_bucket,
                "org"       => $db_org,
                "verifySSL" => FALSE,
                "precision" => InfluxDB2\Model\WritePrecision::NS
            ] );
        } catch ( Exception $e ) {
            cacti_log( "ERROR: " . $e->getMessage(), TRUE, "CereusTransporter" );
            return;
        }

        // Reset the error reporting level to what it was:
        error_reporting( $original_error_reporting );

        $metrics = array();

        $writeApi = $client->createWriteApi( [ "writeType" => WriteType::BATCHING, 'batchSize' => 1000 ] );


        // preparing points
        foreach ( $data_array as $point ) {
            $points = array();
            if ( read_config_option( 'log_verbosity' ) >= POLLER_VERBOSITY_DEBUG ) {
                cacti_log( "DEBUG: Appending the following data to request: [" . $db_type . "] [" . $point[ 'timestamp' ] . "] [" . $point[ 'tags' ][ 'hostname' ] . "] [" . json_encode( $point[ 'tags' ] ) . "] [" . $point[ 'tags' ][ 'type' ] . "] [" . $point[ 'value' ] . "]", TRUE, "CereusTransporter" );
            }
            if ( strlen( $point[ 'metric' ] ) > 0 ) {
                $point_str = "";
                try {
                    if ( array_key_exists( 'metric_text', $point[ 'tags' ] ) ) {
                        $point[ 'tags' ][ 'metric_text' ] = CereusTransporter_cleanTag( $db_type, $point[ 'tags' ][ 'metric_text' ] );
                    }
                    if ( array_key_exists( 'units', $point[ 'tags' ] ) ) {
                        $point[ 'tags' ][ 'units' ] = CereusTransporter_cleanTag( $db_type, $point[ 'tags' ][ 'units' ] );
                    }
                    $points = new InfluxDB\Point(
                        $point[ 'metric' ],
                        $point[ 'value' ],
                        $point[ 'tags' ],
                        array( 'value' => $point[ 'value' ] ),
                        $point[ 'timestamp' ] );

                    //data in array structure
                    $dataArray = [ 'name'  => $point[ 'metric' ],
                                   'tags'  => $point[ 'tags' ],
                                   'value' => $point[ 'value' ],
                                   'time'  => $point[ 'timestamp' ] ];

                    $writeApi->write( $dataArray );
                } catch ( Exception $e ) {
                    cacti_log( "ERROR: " . $e->getMessage(), TRUE, "CereusTransporter" );
                }
            }
        }

        $writeApi->close();
    }